#!/usr/bin/env python3

from redis import Redis
import os
import time
import psycopg2
import json

def get_redis():
   print ("connecting to redis!") 
   if (os.getenv('OS_ENV') == "Z"):
      print ("Connecting to Redis using Z connection string")
      redis_conn = Redis(host="new-redis", db=0, socket_timeout=5)  # on Z 
   else:
      print ("Connecting to Redis using x86 connection string")
      redis_conn = Redis(host="new-redis", db=0, socket_timeout=5, password=os.getenv('redispasswd', "password"))
   print ("connected to redis!") 
   return redis_conn

def connect_postgres(): 
   # Vva1VrSRCqqJnYKH
   dbp=os.getenv('dbpasswd') 
   host=os.getenv('POSTGRES_SERVICE_HOST') 
   db_user = os.getenv('DB_USER', "openshift") 
   db_pass = os.getenv('DB_PASS', "openshift") 
   #db_pass = password=os.getenv('dbpasswd', "password")
   print ("DB password is") 
   print (db_pass) 
   try:
      print ("connecting to the DB") 
      conn = psycopg2.connect ("host={} dbname={} user={} password={}".format("new-postgresql", "postgres", db_user, db_pass))
      print ("Successfully connected to PostGres")

    
      cursor = conn.cursor()
      sqlCreateTable = "CREATE TABLE IF NOT EXISTS votes (id VARCHAR(255) NOT NULL, vote VARCHAR(255) NOT NULL);"
      cursor.execute(sqlCreateTable)
      print ("votes table created") 
      return conn 

   except Exception as e:
      print (e)

def insert_postgres(conn, data): 
    try: 
       cur = conn.cursor() 
       cur.execute("insert into votes values (%s, %s)",
       ( 
          data.get("voter_id"), 
          data.get("vote")
       ))
       conn.commit()  
       print ("row inserted into DB") 

    except Exception as e: 
       conn.rollback()
       print ("error inserting into postgres")  
       print (str(e)) 

def process_votes(db_conn):
    redis = get_redis() 
    while True: 
       try:  
          msg = redis.rpop("votes")
          print(msg)
          if (msg != None): 
             msg_dict = json.loads(msg)
             insert_postgres(db_conn, msg_dict) 
          # will look like this
          # {"vote": "a", "voter_id": "71f0caa7172a84eb"}
          time.sleep(3)        
   
       except Exception as e:
          print(e)

if __name__ == '__main__':
    db_conn = connect_postgres()
    process_votes(db_conn)


